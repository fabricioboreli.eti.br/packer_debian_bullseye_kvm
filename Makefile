# default
PACKER_FILE  = packer.pkr.hcl
OUTPUT_DIR   = output
OUTPUT_NAME  = debian-11.5.0-amd64-pkr.qcow2
OUTPUT       = $(OUTPUT_DIR)/$(OUTPUT_NAME)
ACCELERATOR  = kvm
PACKER_FLAGS = -var accelerator="$(ACCELERATOR)" -var output_dir="$(OUTPUT_DIR)" -var output_name="$(OUTPUT_NAME)"

# default
build: validate $(PACKER_FILE)
	packer build $(PACKER_FLAGS) $(PACKER_FILE)
	tree -h ${OUTPUT_DIR}

validate: $(PACKER_FILE)
	packer validate $(PACKER_FLAGS) $(PACKER_FILE)

clean:
	rm -rf $(OUTPUT_DIR)

really-clean: clean
	rm -rf packer_cache/
