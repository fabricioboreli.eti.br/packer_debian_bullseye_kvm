Packer - Customized Debian 11 Image - QEMU+KVM
==============================================

This packer code builds a qcow2 virtual machine image using a default Debian ISO. 

Cloud Init package is installed to allow further configuration when deploying a VM using this image. 

**Don't forget to change the passwords before using this image in production!**

Default login and password:
```
login: debian
passwd: debian

login: root
passwd: root
```

In order to run this code, you must have some programs installed: 
- qemu + kvm
- hashicorp packer
- make

The produced image and *SHA256SUMS* file will be palced into the _output_ directory.

Licence
-------

MIT 

Author
------

fabricio at automata dot eti dot br 

https://fabricioboreli.eti.br 
